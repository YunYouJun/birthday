# birthday

[![Build Status](https://travis-ci.org/YunYouJun/birthday.svg?branch=master)](https://travis-ci.org/YunYouJun/birthday)

Happy Birthday To You!

## Base

[Codepen-Cake](https://codepen.io/fixcl/pen/nKFDr)

## Use

If you in <https://yunyoujun.github.io/birthday/?name=World&msg=Hello>,
then you can see in this page.

```word
World
Hello
```

| Prop | Type   | Description | Default
| ---  | ---    | ---         | ---
| name | String | who you want to greet | null
| msg  | String | what you want to say | Happy Birthday!

### Setup

```sh
yarn install
```

### Build

```sh
yarn build
```

### View

```sh
# http://localhost:8080
yarn serve
```

## Change Log

- 2019.01.08 | Use vue rewrite
